﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{

    public Room roomPrefab;
    public Bridge bridgePrefab;

    private Room roomInstance;
    private Room newRoom;

    private Bridge bridgeInstance;
    private Bridge newBridge;

    private int roomsGenerated;

    //float spawnDistance = 20;

    private void Start()
    {
        BeginGame();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            RestartGame();
        }
    }

    private void BeginGame()
    {
        roomInstance = Instantiate(roomPrefab) as Room;
        roomInstance.Generate();

        float spawnDistance = roomInstance.sizeX;

        for (int i = 0; i < 50; i++)
        {
            Vector3 roompos = roomInstance.transform.position;

            int roomDirChoice = Random.Range(1, 4);

            if(roomDirChoice == 1) //****THIS ONE WORKS****
            {
                bridgeInstance = Instantiate(bridgePrefab) as Bridge;
                bridgeInstance.Generate();

                Vector3 bridgeDirection = roomInstance.transform.forward;
                Quaternion bridgeRotation = roomInstance.transform.rotation;

                Vector3 bridgePos = roompos + bridgeDirection * spawnDistance;

                newBridge = Instantiate(bridgeInstance, bridgePos, Quaternion.Euler(0, 90, 0)) as Bridge;
                //
                //
                Vector3 roomdirection = roomInstance.transform.forward;
                Quaternion roomRotation = roomInstance.transform.rotation;

                Vector3 roomPos = roompos + roomdirection * (spawnDistance * 2);

                newRoom = Instantiate(roomInstance, roomPos, roomRotation) as Room;

            }
            else if(roomDirChoice == 2) //****THIS ONE WORKS****
            {
                bridgeInstance = Instantiate(bridgePrefab) as Bridge;
                bridgeInstance.Generate();

                Vector3 bridgeDirection = roomInstance.transform.forward;
                Vector3 bridgedirectionBack = bridgeDirection * -1;
                Quaternion bridgeRotation = roomInstance.transform.rotation;

                Vector3 bridgePos = roompos + bridgedirectionBack * spawnDistance;

                newBridge = Instantiate(bridgeInstance, bridgePos, Quaternion.Euler(0, 90, 0)) as Bridge;
                //
                //
                Vector3 roomdirection = roomInstance.transform.forward;
                Vector3 roomdirectionBack = roomdirection * -1;
                Quaternion roomRotation = roomInstance.transform.rotation;


                Vector3 roomPos = roompos + roomdirectionBack * (spawnDistance * 2);

                newRoom = Instantiate(roomInstance, roomPos, roomRotation) as Room;
            }
            else if (roomDirChoice == 3) //****THIS ONE WORKS****
            {
                bridgeInstance = Instantiate(bridgePrefab) as Bridge;
                bridgeInstance.Generate();

                Vector3 bridgeDirection = roomInstance.transform.right;
                Quaternion bridgeRotation = roomInstance.transform.rotation;

                Vector3 bridgePos = roompos + bridgeDirection * spawnDistance;

                newBridge = Instantiate(bridgeInstance, bridgePos, bridgeRotation) as Bridge;
                //
                //
                Vector3 roomdirection = roomInstance.transform.right;
                Quaternion roomRotation = roomInstance.transform.rotation;


                Vector3 roomPos = roompos + roomdirection * (spawnDistance * 2);

                newRoom = Instantiate(roomInstance, roomPos, roomRotation) as Room;
            }
            else if (roomDirChoice == 4) //****THIS ONE WORKS****
            {
                bridgeInstance = Instantiate(bridgePrefab) as Bridge;
                bridgeInstance.Generate();

                Vector3 bridgeDirection = roomInstance.transform.right;
                Vector3 bridgedirectionLeft = bridgeDirection * -1;
                Quaternion bridgeRotation = roomInstance.transform.rotation;

                Vector3 bridgePos = roompos + bridgedirectionLeft * spawnDistance;

                newBridge = Instantiate(bridgeInstance, bridgePos, bridgeRotation) as Bridge;
                //
                //
                Vector3 roomdirection = roomInstance.transform.right;
                Vector3 roomdirectionLeft = roomdirection * -1;
                Quaternion roomRotation = roomInstance.transform.rotation;


                Vector3 roomPos = roompos + roomdirectionLeft * (spawnDistance * 2);

                newRoom = Instantiate(roomInstance, roomPos, roomRotation) as Room;
            }
            roomInstance = newRoom;
            


            //spawnDistance = spawnDistance + 20f;
        }
    }

    private void RestartGame()
    {
        Destroy(roomInstance.gameObject);
        BeginGame();
    }
}
