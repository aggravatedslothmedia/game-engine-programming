﻿using UnityEngine;
using System.Collections;

public class Room : MonoBehaviour {

    public int sizeX = Random.Range(5, 15);
    public int sizeZ = 10;

    public RoomCell cellPrefab;

    private RoomCell[,] cells;

    public void Awake()
    {
        BoxCollider bc = gameObject.AddComponent<BoxCollider>();
        bc.size = new Vector3(sizeX, 0, sizeZ);
        bc.isTrigger = true;
    }

    public void Generate()
    {
        cells = new RoomCell[sizeX, sizeZ];
        for (int x = 0; x < sizeX; x++)
        {
            for (int z = 0; z < sizeZ; z++)
            {
                CreateCell(x, z);
            }
        }
    }

    private void CreateCell(int x, int z)
    {
        RoomCell newCell = Instantiate(cellPrefab) as RoomCell;
        cells[x, z] = newCell;
        newCell.name = "Room Cell " + x + ", " + z;
        newCell.transform.parent = transform;
        newCell.transform.localPosition = new Vector3(x - sizeX * 0.5f + 0.5f, 0f, z - sizeZ * 0.5f + 0.5f);
        if(x == 0 && z <= 9 || x <= 9 && z == 9 || x == 9 && z <= 9 || x <=9 && z == 0)
        {
            newCell.tag = "RoomEdge";
        }
        else
        {
            newCell.tag = "RoomCell";
        }
        if (newCell.tag == "RoomEdge")
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.position = newCell.transform.position;
            cube.transform.parent = newCell.transform;
        }
    }

}
