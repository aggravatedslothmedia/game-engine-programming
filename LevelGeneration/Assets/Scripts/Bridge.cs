﻿using UnityEngine;
using System.Collections;

public class Bridge : MonoBehaviour
{
    public int sizeX = Random.Range(10, 15);
    public int sizeZ = 3;

    public BridgeCell cellPrefab;

    private BridgeCell[,] cells;

    public void Awake()
    {
        BoxCollider bc = gameObject.AddComponent<BoxCollider>();
        bc.size = new Vector3(sizeX, 0, sizeZ);
        bc.isTrigger = true;
    }

    public void Generate()
    {
        cells = new BridgeCell[sizeX, sizeZ];
        for (int x = 0; x < sizeX; x++)
        {
            for (int z = 0; z < sizeZ; z++)
            {
                CreateCell(x, z);
            }
        }
    }

    private void CreateCell(int x, int z)
    {
        BridgeCell newCell = Instantiate(cellPrefab) as BridgeCell;
        cells[x, z] = newCell;
        newCell.name = "Bridge Cell " + x + ", " + z;
        newCell.transform.parent = transform;
        newCell.transform.localPosition = new Vector3(x - sizeX * 0.5f + 0.5f, 0f, z - sizeZ * 0.5f + 0.5f);
        if (x <= 10 && z == 0 || x <= 10 && z == 2)
        {
            newCell.tag = "BridgeEdge";
        }
        else
        {
            newCell.tag = "BridgeCell";
        }
        if (newCell.tag == "BridgeEdge")
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.position = newCell.transform.position;
            cube.transform.parent = newCell.transform;
        }
    }
}
